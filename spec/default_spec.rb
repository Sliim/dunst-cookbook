# -*- coding: utf-8 -*-

require 'rspec/expectations'
require 'chefspec'
require 'chefspec/berkshelf'

ChefSpec::Coverage.start! { add_filter 'dunst' }

require 'chef/application'

describe 'dunst::default' do
  context 'without user configuration' do
    subject { ChefSpec::SoloRunner.new.converge(described_recipe) }

    it 'does install dunst package' do
      expect(subject).to install_package('dunst')
    end

    it 'does not create dunst user directory' do
      expect(subject).not_to create_directory('/home//.config/dunst')
    end

    it 'does not create dunst config file from template' do
      expect(subject).not_to create_template('/home//.config/dunst/dunstrc')
    end
  end

  context 'with user configuration' do
    let(:subject) do
      ChefSpec::ServerRunner.new do |node|
        node.set['dunst']['user'] = 'user'
      end.converge(described_recipe)
    end

    it 'does install dunst package' do
      expect(subject).to install_package('dunst')
    end

    it 'does create dunst user directory' do
      expect(subject).to create_directory('/home/user/.config/dunst')
        .with(owner: 'user',
              group: 'user',
              recursive: true)
    end

    it 'does create dunst config file from template' do
      config_file = '/home/user/.config/dunst/dunstrc'
      matches = [start_with('# Generated by Chef!'),
                 /^\s+font = Monospace 8$/,
                 /^\s+allow_markup = yes$/,
                 /^\s+format = "<b>%s<\/b>\\n%b"$/,
                 /^\s+sort = yes$/,
                 /^\s+indicate_hidden = yes$/,
                 /^\s+alignment = left$/,
                 /^\s+bounce_freq = 0$/,
                 /^\s+show_age_threshold = 60$/,
                 /^\s+word_wrap = yes$/,
                 /^\s+ignore_newline = no$/,
                 /^\s+geometry = "300x5-10\+20"$/,
                 /^\s+shrink = no$/,
                 /^\s+transparency = 0$/,
                 /^\s+idle_threshold = 120$/,
                 /^\s+monitor = 0$/,
                 /^\s+follow = mouse$/,
                 /^\s+sticky_history = yes$/,
                 /^\s+history_length = 20$/,
                 /^\s+show_indicators = yes$/,
                 /^\s+line_height = 0$/,
                 /^\s+separator_height = 2$/,
                 /^\s+padding = 8$/,
                 /^\s+horizontal_padding = 8$/,
                 /^\s+separator_color = frame$/,
                 /^\s+startup_notification = false$/,
                 %r{^\s+dmenu = /usr/bin/dmenu -p dunst:$},
                 %r{^\s+browser = /usr/bin/firefox -new-tab$},
                 /^\s+icon_position = off$/,
                 # rubocop:disable Metrics/LineLength
                 %r{^\s+icon_folders = /usr/share/icons/gnome/16x16/status/:/usr/share/icons/gnome/16x16/devices/$},
                 /^\s+width = 2$/,
                 /^\s+color = "#AAAAAA"$/,
                 /^\s+close = ctrl\+space$/,
                 /^\s+close_all = ctrl\+shift\+space$/,
                 /^\s+history = ctrl\+grave$/,
                 /^\s+context = ctrl\+shift\+period$/,
                 /^\s+background = "#222222"$/,
                 /^\s+foreground = "#888888"$/,
                 /^\s+timeout = 10$/,
                 /^\s+background = "#285577"$/,
                 /^\s+foreground = "#FFFFFF"$/,
                 /^\s+timeout = 10$/,
                 /^\s+background = "#900000"$/,
                 /^\s+foreground = "#FFFFFF"$/,
                 /^\s+timeout = 0$/]

      expect(subject).to create_template(config_file)
        .with(owner: 'user',
              group: 'user',
              source: 'dunstrc.erb')

      matches.each do |m|
        expect(subject).to render_file(config_file).with_content(m)
      end
    end
  end
end
