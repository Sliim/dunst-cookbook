dunst CHANGELOG
===============

This file is used to list changes made in each version of the dunst cookbook.

0.1.0
-----
- Initial release of dunst cookbook
